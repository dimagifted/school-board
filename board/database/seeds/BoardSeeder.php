<?php

use Illuminate\Database\Seeder;
use App\Entities\Student;
use App\Entities\Board;
use App\Entities\Grade;

class BoardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Board::insert([[
            'name' => 'CSM',
            'response_type' => 'json',
        ],[
            'name' => 'CSMB',
            'response_type' => 'xml',
        ]]);

        factory(Student::class, 50)->create()->each(function ($student) {
            $student->grades()->saveMany(factory(Grade::class,4)->make());
            $student->board_id=rand(1,2);
            $student->save();

        });
    }
}
