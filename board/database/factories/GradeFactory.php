<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Grade;


$factory->define(Grade::class, function () {
    return [
        'grade' => rand(1,10),
    ];
});
