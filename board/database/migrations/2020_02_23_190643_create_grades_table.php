<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('grade');
            $table->bigInteger('student_id')->unsigned();
            $table->timestamps();

            $table->index('student_id');
            $table->foreign('student_id')->references('id')
                ->on('students')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('grades', function (Blueprint $table) {

            $table->dropForeign(['student_id']);
            $table->dropIndex(['student_id']);

        });
        Schema::dropIfExists('grades');
    }
}
