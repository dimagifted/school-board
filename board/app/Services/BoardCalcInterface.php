<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 24.02.20
 * Time: 17:09
 */

namespace App\Services;


interface BoardCalcInterface
{
    const FAIL='fail';
    const PASS='pass';

    /**
     * @return string
     */
    public function checkPass():string ;
}