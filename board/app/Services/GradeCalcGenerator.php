<?php

namespace App\Services;


use App\Entities\Board;

class GradeCalcGenerator
{

    /**
     * @param string $boardType
     * @param int $averageGrade
     * @param array $grades
     * @return BoardCalcInterface
     */
    public function generateBoardCalc(string $boardType, int $averageGrade, array $grades): BoardCalcInterface
    {
        switch ($boardType) {
            case Board::CSM_TYPE:
                return (new BoardCSMCalc($averageGrade));
                break;
            case Board::CSMB_TYPE:
                return (new BoardCSMBCalc($grades));
                break;
            default:
                throw new \RuntimeException('unsupported board!');
        }
    }
}
