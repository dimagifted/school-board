<?php

namespace App\Services;


class BoardCSMCalc implements BoardCalcInterface
{
    private const AVERAGE_GRADE_TO_PASS=7;
    /**
     * @var int
     */
    private $average;

    public function __construct(int $average)
    {
        $this->average = $average;
    }

    public function checkPass(): string
    {
        $finalResult = BoardCalcInterface::FAIL;
        if ($this->average >= self::AVERAGE_GRADE_TO_PASS) {
            $finalResult = BoardCalcInterface::PASS;
        }
        return $finalResult;
    }
}