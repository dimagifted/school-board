<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 24.02.20
 * Time: 17:08
 */

namespace App\Services;


class BoardCSMBCalc implements BoardCalcInterface
{
    private const GRADE_TO_PASS=8;
    /**
     * @var array
     */
    private $grades;

    public function __construct(array $grades)
{
    $this->grades = $grades;
}

    public function checkPass(): string
    {
        $finalResult = BoardCalcInterface::FAIL;
        if (max($this->grades) > self::GRADE_TO_PASS) {
            $finalResult = BoardCalcInterface::PASS;
        }

        return $finalResult;
    }
}