<?php


namespace App\Repositories;


use App\Entities\Student;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface StudentRepositoryInterface
{

    /**
     * @return Collection
     */
    public function getAll() :Collection;

    /**
     * @return Collection
     */
    public function getAllPaginated() :LengthAwarePaginator;

    /**
     * @param int $id
     * @return Student
     * @throws EntityNotFoundException
     */
    public function getById(int $id) :Student;

}