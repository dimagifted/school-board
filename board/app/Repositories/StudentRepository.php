<?php

namespace App\Repositories;


use App\Entities\Student;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class StudentRepository implements StudentRepositoryInterface
{
    /**
     * @var Student
     */
    private $student;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    public function getAll(): Collection
    {
        return $this->student->all();
    }

    public function getAllPaginated($perPage = 15): LengthAwarePaginator
    {
        return $this->student->newQuery()->paginate($perPage);
    }

    public function getById(int $id): Student
    {
        $student = $this->student->newQuery()->where('id', $id)
            ->with('board', 'grades')
            ->first();
        if (empty($student)) {
            throw new EntityNotFoundException('student does not exist');
        }
        return $student;
    }
}