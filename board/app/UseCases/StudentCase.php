<?php

namespace App\UseCases;


use App\Entities\Student;
use App\Repositories\StudentRepository;
use App\Repositories\StudentRepositoryInterface;
use App\Services\GradeCalcGenerator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class StudentCase
{
    /**
     * @var StudentRepository
     */
    private $studentRepository;
    /**
     * @var GradeCalcGenerator
     */
    private $gradeCalcGenerator;

    public function __construct(StudentRepositoryInterface $studentRepository, GradeCalcGenerator $gradeCalcGenerator)
    {
        $this->studentRepository = $studentRepository;
        $this->gradeCalcGenerator = $gradeCalcGenerator;
    }

    public function getStudents() :LengthAwarePaginator
    {
        return $this->studentRepository->getAllPaginated();

    }

    public function getStudentById(int $id) :Student
    {
        return $this->studentRepository->getById($id);

    }

    public function caclGrades(int $id): array
    {
        $student = $this->getStudentById($id);
        $grades = array_column($student->grades->toArray(), 'grade');
        $averageGrade = array_sum($grades) / count($grades);

        $finalResult = ($this->gradeCalcGenerator->generateBoardCalc($student->board->name, $averageGrade, $grades))->checkPass();

        return [
            'id' => $student->id,
            'name' => $student->name,
            'pass' => $finalResult,
            'grades' => $grades,
            'average' => $averageGrade,
        ];
    }
}