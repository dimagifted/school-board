<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 24.02.20
 * Time: 12:22
 */

namespace App\Http\Controllers;



use App\UseCases\StudentCase;

class StudentController extends Controller
{

    /**
     * @var StudentCase
     */
    private $studentService;

    public function __construct(StudentCase $studentService)
    {
        $this->studentService = $studentService;
    }

    public function index()
    {
        return view('students.list', ['students' => $this->studentService->getStudents()]);
    }

    public function view($id)
    {
        $student = $this->studentService->getStudentById($id);
        return self::getResponseType($student->board->response_type, $this->studentService->caclGrades($id));
    }


    public static function getResponseType($boardTypeResponse, $responseData)
    {
        switch ($boardTypeResponse) {
            case "json":
                return response()->json($responseData);
                break;
            case "xml":
                return response()->xml($responseData);
                break;
            default:
                return response()->json($responseData);
        }
    }

}