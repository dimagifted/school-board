@extends('app')

<div class="container">
    @foreach ($students as $student)
        <a href="{{route('student.view',['id'=>$student->id])}}" target="_blank">{{ $student->name }}, {{ $student->board->name }}</a><br>
    @endforeach
</div>

{{ $students->links() }}