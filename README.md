## Usage

Run commands
 - docker-compose build
 - docker-compose up -d
 
Can be accessed at [http://localhost:8080](http://localhost:8080) 

## init
Run commands
 - docker-compose exec php-cli  composer install
 - docker-compose exec php-cli php artisan migrate:fresh --seed
