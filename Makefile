docker-up:
	docker-compose up -d
docker-down:
	docker-compose down
composer-install:
	docker-compose exec php-cli  composer install
init:
	docker-compose exec php-cli php artisan migrate:fresh --seed
